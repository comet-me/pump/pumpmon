#!/usr/bin/env python

#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Wrapper of the /sys/class/gpio interface provided by gpio-sunxi module.

Tested with 3.4.103-00033-g9a1cd03-dirty on Olimex OlinuXino micro A20

Python 3
"""

import sys
from time import time
import select
from os import path
from subprocess import check_output, PIPE
from argparse import ArgumentParser
from shutil import which


DEBUG = False


def in_between(start, end, lines):
    t = 0
    for l in lines:
        if l == start:
            t += 1
        if t == 1 and l == end:
            t += 1
        if t == 1:
            yield l



ON_BOARD = which('bin2fex') is not None

if ON_BOARD:
    def bin2fex():
        return check_output(['bin2fex', '/boot/script.bin'], stderr=PIPE).split(b'\n')

else:
    def bin2fex():
        return [
            b'[gpio_para]',
            b'gpio_pin_1 = port:PG00<0><default><default><default>',
            b'gpio_pin_2 = port:PG01<0><default><default><default>',
            b'',
        ]

def init_fex():
    lines = bin2fex()
    data = list(in_between(b'[gpio_para]', b'', lines))[1:]
    # b'gpio_pin_48 = port:PI00<0><default><default><default>',
    def parse_line(l):
        lhs, rhs = l.split(b'=', 1)
        lhs = lhs.strip()
        if not lhs.startswith(b'gpio_pin_'):
            if DEBUG:
                print("ignoring {l}".format(l=l))
            return None
        gpio_number = int(lhs[len(b'gpio_pin_'):])
        rhs = rhs.strip()
        port_str, rest = rhs.split(b':', 1)
        assert port_str == b'port'
        parts = rest.split(b'<')
        port_name = parts[0]
        assert all(p[-1] == b'>'[0] for p in parts[1:]), "parts: {}".format(str(parts[1:]))
        assert len(port_name) == 4 and port_name[0] == b'P'[0]
        port_letter = chr(port_name[1])
        port_pin = int(port_name[2:])
        return (gpio_number, port_letter, port_pin, (p[:-1] for p in parts[1:]))
    return [x for x in [parse_line(l) for l in data] if x is not None]


class Fex:

    data = init_fex()

    d_portpin2pin = {
        'P{letter}{port_pin}'.format(letter=port_letter.upper(), port_pin=port_pin):
        str(gpio_number) for
            (gpio_number, port_letter, port_pin, bla) in data}

    d_portpin2filename = {
        'P{letter}{port_pin}'.format(letter=port_letter.upper(), port_pin=port_pin):
        'gpio{gpio_number}_p{port_letter}{port_pin}'.format(
            port_letter=port_letter.lower(), gpio_number=gpio_number, port_pin=port_pin) for
            (gpio_number, port_letter, port_pin, bla) in data}

    @classmethod
    def portpin2filename(clz, port_pin):
        return clz.d_portpin2filename[port_pin]

    @classmethod
    def portpin2pin(clz, port_pin):
        return clz.d_portpin2pin[port_pin]


# Valid values to write to <gpio_dir>/edge
RISING = 'rising'
FALLING = 'falling'

# Valid values to write to <gpio_dir>/direction
INPUT = "in"
OUTPUT = "out"


class GPIO:
    BASE = '/sys/class/gpio'
    def __init__(self, port_pin, direction=INPUT, interrupt=None):
        self.port_pin = port_pin
        self.pin = Fex.portpin2pin(port_pin)
        self.basename = Fex.portpin2filename(port_pin)
        print("Instantiated gpio {pin} ({port_pin}) corresponding to {basename}".format(**self.__dict__))
        self.export()
        self.interrupt = interrupt
        assert interrupt in [RISING, FALLING, None]
        if interrupt is not None:
            self.set_interrupt()
        assert direction in [INPUT, OUTPUT]
        self.direction = direction
        self._write_direction()

    @property
    def directory(self):
        return path.join(self.BASE, self.basename)

    @property
    def value_filename(self):
        return path.join(self.directory, 'value')

    def _write_direction(self):
        with open(path.join(self.directory, 'direction'), 'w') as fd:
            fd.write(self.direction)

    def export(self):
        if path.exists(self.directory):
            return
        with open(path.join(self.BASE, 'export'), 'w') as fd:
            fd.write('{}\n'.format(self.pin))
        assert path.exists(self.directory), "export failed for pin {pin}: {directory} missing".format(**self.__dict__)

    def set_interrupt(self):
        with open(path.join(self.directory, 'edge'), 'w') as fd:
            fd.write('{}\n'.format(self.interrupt))

    def output(self, val):
        with open(self.value_filename, 'w') as fd: # TODO - can we keep a descriptor?
            fd.write(str(val))

    def input(self):
        with open(self.value_filename, 'r') as fd: # TODO - can we keep a descriptor?
            return fd.read()

    def poll_iter(self):
        poll = select.poll()
        fd = open(self.value_filename, 'r')
        poll.register(fd, select.POLLPRI)
        while True:
            for _fd, event in poll.poll(3000): # milliseconds
                data = fd.read(3) # TODO - read the actual amount needed
                yield # caller is responsible for interpreting the event


def dump_fex():
    print('\n'.join(map(str, Fex.data)))
    print('\n'.join(['{}: {}'.format(k, v) for k, v in Fex.d.items()]))


def main():
    parser = ArgumentParser()
    parser.add_argument('--dump', action='store_true', default=False, help='dump FEX GPIO map')
    parser.add_argument("-w", dest="direction", type=lambda s: (OUTPUT, s), help="write given value")
    parser.add_argument("-p", dest="pin", choices=Fex.d.keys())
    parser.add_argument("-r", dest="direction", action="store_const", const=(INPUT,None))
    parser.add_argument("--poll", dest="direction", action="store_const", const=(INPUT, 'poll'))
    args = parser.parse_args(sys.argv[1:])
    if args.dump:
        dump_fex()
        raise SystemExit
    if args.direction is None or args.pin is None:
        print("-p is required and one of -w <value> or -r is required")
        parser.print_help()
        raise SystemExit
    direction, value = args.direction
    interrupt = RISING if direction == INPUT and value == 'poll' else None
    gpio = GPIO(pin=args.pin, direction=direction, interrupt=interrupt)
    if value is not None and value is not 'poll':
        gpio.output(value)
    else:
        if interrupt:
            t0 = time()
            last_print = t0 - 10.0
            dt_print = 1.0
            count = 0
            for t in gpio.poll_iter():
                dt = t - t0
                count += 1
                if t > last_print + dt_print:
                    print('{dt}: {count}'.format(dt=dt, count=count))
                    count = 0
                    last_print = t
        else:
            print(gpio.input())


if __name__ == '__main__':
    main()
