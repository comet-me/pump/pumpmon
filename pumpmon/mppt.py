#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from collections import defaultdict
import re

field_re = re.compile(b'[^ :]*:')

class MPPTLogParser:
    """
    Stream parser. Should handle any segmentation of the stream.
    """
    def __init__(self):
        self.pending = b''

    def _parse_fields_and_key_values(self, line):
        """
        Trying to accomodate as many future changes as possible. So this function
        expects a message of the form:
        [<field>: [(<key>=<value>|<unkeys>),]]

        and returns:
        {field: {key: value}}

        It only flattenes a case where there is a single unkeyed value:
        {field: unkeyed}
        """
        fields = [(sre.group()[:-1], sre.span()) for sre in field_re.finditer(line)]
        rest = [line[f1_end:f2_start] for (f1, (f1_start, f1_end)), (f2, (f2_start, f2_end))
                in zip(fields, fields[1:] + [('END', (len(line), len(line)))])]
        stripall = lambda xs: [x.strip() for x in xs]
        d = {}
        for (field, span), line in zip(fields, rest):
            data = {}
            unkeyed = []
            for x in line.split(b','):
                parts = stripall(x.split(b'=', 1))
                if len(parts) == 2:
                    data[parts[0]] = parts[1]
                else:
                    unkeyed.append(b'|'.join(parts))
            if len(unkeyed) > 0:
                if len(unkeyed) == 1:
                    unkeyed = unkeyed[0] # left here to support more unkeyed elements possibly
                if len(data) == 0:
                    data = unkeyed
                else:
                    # no data fits this right now, but in case it does in the
                    # future
                    data[field] = unkeyed
            d[field] = data
        if sum([len(x) for x in d.values()]) > 0:
            return d
        return None

    def _parse_line(self, line):
        # MPPT:    Vi=36.30, Ii=5.92,   Vo=37.00, Io=5.65,   Pi=214, Po=208,   D=6, n=97.17, Tmp.=35	 Flgs: OV=0, OT=0, TF=0, OFF=0, UV=0	 Time: 1860544
        # "MPPT:" is actually a state. It can be VmpSeek, HALT. So don't use it as a prefix at all - but that is done in a higher level than this function
        # Any fields that aren't "Time" or "Flgs" get's flattened, and the field keys
        # are gathered into a "State" key. If there is just one, it is flattened.
        t = datetime.now().timestamp() * 1000
        d = self._parse_fields_and_key_values(line)
        # Special case for 'Compiled' line
        if d is None:
            return d
        if b'Compiled' in d.keys():
            return {b'Line': line, b'RecordedTime': t}
        d[b'RecordedTime'] = t
        keys = [k for k, v in d.items() if k not in [b'Time', b'Flgs'] and isinstance(v, dict)]
        for pk in keys:
            for k, v in d[pk].items():
                if k in d:
                    if isinstance(d[k], list):
                        d[k].append(v)
                    else:
                        d[k] = [d[k], v]
                else:
                    d[k] = v
            del d[pk]
        if len(keys) > 0:
            d[b'State'] = keys if len(keys) > 1 else keys[0]
        return d

    def parse_message(self, msg):
        assert isinstance(msg, bytes)
        ret = []
        msg = self.pending + msg
        lines = msg.split(b'\n')
        self.pending = lines[-1]
        for line in lines[:-1]:
            msg = self._parse_line(line)
            if msg is not None:
                ret.append(msg)
        return ret

def mppt_set_print_loop_milli(s, t):
    s.write('p{}\r\n'.format(t).encode())


def mppt_set_control_loop_milli(s, t):
    s.write('t{}\r\n'.format(t).encode())


def mppt_get_information(s):
    """
    sends get information command. NOTE: does not read the response!
    """
    s.write(b'i\r\n')

