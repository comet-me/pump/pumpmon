#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from time import sleep
try:
    import pigpio
except RuntimeError:
    pass


INPIN = 17 # BCM 17, board 11


def utc_millis():
    return datetime.now().timestamp() * 1000


def pulses_per_second_gen():
    """
    generator of puleses from INPIN. yields dictionary
    with keys:
    'time' - milliseconds since epoch in UTC
    'count' - events per second (not milliseconds)
    """
    pi = pigpio.pi()
    pi.set_mode(INPIN, pigpio.INPUT)
    pi.set_pull_up_down(INPIN, pigpio.PUD_UP)

    cb = pi.callback(INPIN)
    last = utc_millis()
    while True:
        sleep(1)
        t = utc_millis()
        count = cb.tally()
        cb.reset_tally()
        yield dict(time=t, count=count)

