#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import csv
from datetime import datetime
from os.path import join, exists
from os import makedirs


def ensure_dir_exists(d):
    if not exists(d):
        makedirs(d)


class DailyCSVWriter:
    """
    Write to a file with the given prefix and given date suffix.
    The suffix is recalculated on every line written, and if a new one
    is produced, due to the change in date, a new file is written to.
    """

    # Format of produced filenames
    date_format = '%Y%m%d_%H%M'

    def __init__(self, prefix, fields, suffix='.csv',
                 writer_args=None, writer_kw=None):
        if writer_args is None:
            writer_args = []
        if writer_kw is None:
            writer_kw = {}
        self.writer_args = writer_args
        self.writer_kw = writer_kw
        self.prefix = prefix
        self.fields = fields
        self.suffix = '.csv'
        self.fd = None
        self.filename = None
        self.writer = None

        # NOTE: headers are written either:
        #  - by user request, including the initial header
        #  - when we decide to start a new file, i.e. rotate

    def _append_to_file(self, filename):
        """easy to patch point for mocking"""
        return open(filename, 'a+')

    def _now(self):
        """easy to patch point for mocking"""
        return datetime.now()

    def _overwrite_writer(self):
        """
        This will implicitly reuse or create a new file as determined by
        the self.date_format ; the default format includes minutes so it
        will most likely result in a new file, and at worst a minute of lost data.
        """
        now = self._now()
        date_str = now.strftime(self.date_format)
        year, month, day = '{:04}'.format(now.year), '{:02}'.format(now.month), '{:02}'.format(now.day)
        basename = '{prefix}_{date_str}{suffix}'.format(prefix=self.prefix, date_str=date_str, suffix=self.suffix)
        directory = join(year, month, day)
        ensure_dir_exists(directory)
        self.filename = join(directory, basename)
        self.file_date = self.current_date()
        self.fd = self._append_to_file(self.filename)
        self.writer = csv.DictWriter(self.fd, self.fields, *self.writer_args, **self.writer_kw)
        self.header_written = False

    def current_date(self):
        return self._now().date()

    def writerow(self, d):
        if self.writer is None or self.current_date() != self.file_date:
            self._overwrite_writer()
        self._ensure_header()
        if not isinstance(d, dict):
            d = dict(zip(self.fields, d))
        self.writer.writerow(d)
        self.fd.flush() # this can be milliseconds

    def writeheader(self):
        """
        Marks a flag so the next row will cause a new file
        to be created. But does nothing immediately.
        """
        self.writer = None

    def _ensure_header(self):
        if self.header_written:
            return
        self.header_written = True
        self.writer.writeheader()


def from_filename(filename, fields, *args, **kw):
    """
    parses @filename into prefix
    """
    base, suffix = filename.rsplit('.', 1)
    underscore_parts = base.split('_', 1)
    prefix = underscore_parts[0]
    return DailyCSVWriter(prefix=prefix, fields=fields, suffix=suffix, writer_args=args, writer_kw=kw)

