#!/usr/bin/env python

#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sys import exit
from serial import SerialException, serial_for_url
from os import getcwd, chdir, path
from os.path import realpath, exists, dirname
from argparse import ArgumentParser
from emolog.emotool.main import main as emo_main
from shutil import copy, SameFileError


module_dir = dirname(__file__)


def main():
    p = ArgumentParser()
    p.add_argument('--dir', dest='d', help='runtime directory', default=getcwd())
    p.add_argument('--fake', choices=['gen', 'host'], default=None, help='testing helper')
    p.add_argument('--serial', default='hwgrep://0403:6010')
    p.add_argument('--elf', help='ELF executable running on the controller',
        default=realpath(path.join(module_dir, 'pump_drive_tiva.out')))
    p.add_argument('--vars', help='Variables definitions for recording',
        default=realpath(path.join(module_dir, 'vars.csv')))
    args = p.parse_args()

    if args.fake is None:
        try:
            s = serial_for_url(args.serial)
        except:
            print("serial port not available, exiting")
            raise SystemExit
    elf_host = realpath('linux_pump_drive')
    emo_args = [
        '--runtime', '0',
        '--out', 'pump.csv',
        '--csv-factory', 'pumpmon.rotatingcsv.from_filename',
        '--varfile', args.vars,
        '--no-processing']
    if args.fake == 'gen':
        emo_args.extend(['--fake', 'gen', '--runtime', '0.1'])
    elif args.fake == 'host':
        if not exists(elf_host):
            print("missing {}, cannot run".format(elf_host))
            raise SystemExit
        emo_args.extend(['--fake', elf_host])
    else:
        if not exists(args.elf):
            print("missing {elf}, cannot run".format(elf=args.elf))
            raise SystemExit
        emo_args.extend(['--elf', args.elf, '--serial', args.serial])
    print('running: {}'.format(emo_args))
    local_machine_config_filename = path.join(args.d, 'local_machine_config.ini')
    if not exists(local_machine_config_filename):
        with open(local_machine_config_filename, 'w+') as fd:
            fd.write('[folders]\noutput_folder=.\n')
    chdir(args.d)
    emo_main(emo_args)


if __name__ == '__main__':
    main()
