#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from .gpio.pygpiosunxi import GPIO, RISING


INPIN = 'PH0'


def utc_millis():
    return datetime.now().timestamp() * 1000


def pulsegen():
    """
    generator of puleses from INPIN. yields dictionary
    with keys:
    'time' - milliseconds since epoch in UTC
    'rate' - events per second (not milliseconds)
    """
    inp = GPIO(port_pin=INPIN, interrupt=RISING)
    it = inp.poll_iter()
    next(it)
    last = utc_millis()
    for _t in it:
        t = utc_millis()
        dt = (t - last)
        yield dict(time=t, dt=dt)
        last = t

