#!/usr/bin/env python

#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from os import getcwd, chdir, environ
from argparse import ArgumentParser

from .rotatingcsv import DailyCSVWriter


from .rpigpio import pulses_per_second_gen

# Flow Meter
# Hertz to Liters Per Minute convertion
# If environment variable contains an illegal value abort during import
FLOW_METER_HERTZ_TO_LPM = float(environ.get('PUMPMON_FLOW_HERTZ_TO_LPM', 123.16 * 60))


def pulses_per_second():
    """
    Return rate based on number of pulses per second, using the
    pulses timestamps (doesn't use the time function)

    rate = n_pulses * flow_per_pulse
    """
    it = pulses_per_second_gen()
    first = next(it)
    time = first['time']
    for d in it:
        dtime = d['time']
        dt = dtime - time
        hertz = d['count'] / dt * 1000.0
        rate = hertz * FLOW_METER_HERTZ_TO_LPM
        period_d = {
            'time': dtime,
            'rate': rate,
            'pulses': d['count'],
            'dt': dt,
        }
        yield period_d
        time = dtime


def record():
    writer = DailyCSVWriter(prefix='flow', fields=['time', 'rate', 'dt', 'pulses'])
    for d in pulses_per_second():
        writer.writerow(d)


def debugdump():
    for d in pulses_per_second():
        print(repr(d))


def main():
    parser = ArgumentParser()
    parser.add_argument('--dir', dest='d', default=getcwd(),
                        help='runtime directory, stores csv files')
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()
    chdir(args.d)
    if args.debug:
        debugdump()
    else:
        record()


if __name__ == '__main__':
    main()
