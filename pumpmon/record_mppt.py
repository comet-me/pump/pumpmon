#!/usr/bin/env python

#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from argparse import ArgumentParser
from serial import serial_for_url
import serial
from time import sleep


DEFAULT_MPPT_BAUDRATE = 115200


serial.protocol_handler_packages.append('serialfile')


def dict_consume(d, field):
    val = d
    l = []
    if isinstance(field, (tuple, list)):
        parts = field
    else:
        parts = field.split(b'.')
    for f in parts:
        if f in val:
            l.append((val, f))
            val = val[f]
    for di, key in reversed(l):
        if not isinstance(di[key], dict) or len(di[key]) == 0:
            del di[key]
    if len(l) == len(parts):
        return val
    return None


def try_decode(x):
    try:
        return x.decode()
    except Exception:
        return x


def main():
    parser = ArgumentParser()
    parser.add_argument('--dir', dest='d', default=os.getcwd())
    parser.add_argument('--url', default='hwgrep://0403:6015')
    parser.add_argument('--baudrate', default=DEFAULT_MPPT_BAUDRATE)
    parser.add_argument('--printmilli', default=1000)
    parser.add_argument('--loopmilli', default=1000)
    args = parser.parse_args()
    if not os.path.isdir(args.d):
        parser.print_help()
        raise SystemExit
    try:
        s = serial_for_url(args.url, baudrate=args.baudrate)
    except Exception as e:
        print(e)
        raise SystemExit
    try:
        os.chdir(args.d)
    except Exception:
        print('cannot change dir to {d}'.format(d=args.d))
        raise SystemExit

    from .mppt import (MPPTLogParser, mppt_set_print_loop_milli,
         mppt_set_control_loop_milli, mppt_get_information)
    from .rotatingcsv import DailyCSVWriter

    mppt_parser = MPPTLogParser()
    dict_keys = [
        b'RecordedTime',
        b'Time',
        b'State',
        b'Vi',
        b'Ii',
        b'Vo',
        b'Io',
        b'Pi',
        b'Po',
        b'D',
        b'n',
        (b'Tmp.',),
        b'Flgs.OV',
        b'Flgs.OT',
        b'Flgs.TF',
        b'Flgs.OFF',
        b'Flgs.UV',
    ]
    special = {
        (b'Tmp.',): b'Tmp'
    }
    fields = [special.get(x, x) for x in dict_keys]
    UNPARSED = b'Unparsed'
    UNPARSED_str = UNPARSED.decode()
    csv_fields = [x.decode() for x in fields + [UNPARSED]]
    prefix='mppt'
    writer = DailyCSVWriter(prefix=prefix, fields=csv_fields)
    # TODO - sleep here by trial and error Why is it required? what is the real condition?
    sleep(3)
    try:
        mppt_set_print_loop_milli(s, args.printmilli)
        mppt_set_control_loop_milli(s, args.loopmilli)
        mppt_get_information(s)
    except:
        print("Write exception, continuing with read only")
    # Request information (EEPROM and Compile date)
    writer.writeheader()
    while True:
        line = s.readline()
        if len(line) == 0:
            break
        msgs = mppt_parser.parse_message(line)
        for m in msgs:
            d = {csv_k: try_decode(dict_consume(m, k)) for csv_k, k in zip(csv_fields, dict_keys)}
            d[UNPARSED_str] = repr(m) if len(m) > 0 else None
            writer.writerow(d)


if __name__ == '__main__':
    main()

