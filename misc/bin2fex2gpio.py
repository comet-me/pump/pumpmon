# coding: utf-8
#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from subprocess import check_output
check_output('bin2fex /boot/script.bin')
check_output(['bin2fex', '/boot/script.bin'])
check_output(['bin2fex', '/boot/script.bin']).split('\n')
check_output(['bin2fex', '/boot/script.bin']).split(b'\n')
in_between('[gpio_para]', '', check_output(['bin2fex', '/boot/script.bin']).split(b'\n'))
def in_between(start, end, lines):
    t = 0
    for l in lines:
        if l == start:
            t += 1
        if l == end:
            t += 1
        if l == 1:
            yield l

in_between('[gpio_para]', '', check_output(['bin2fex', '/boot/script.bin']).split(b'\n'))
list(in_between('[gpio_para]', '', check_output(['bin2fex', '/boot/script.bin']).split(b'\n')))
list(in_between(b'[gpio_para]', b'', check_output(['bin2fex', '/boot/script.bin']).split(b'\n')))
def in_between(start, end, lines):
    t = 0
    for l in lines:
        if l == start:
            t += 1
        if l == end:
            t += 1
        if t == 1:
            yield l

list(in_between(b'[gpio_para]', b'', check_output(['bin2fex', '/boot/script.bin']).split(b'\n')))
list(in_between('a','b',['a',2,3,4,'b']))
lines= check_output(['bin2fex', '/boot/script.bin']).split(b'\n')
lines[0]
[x for x in lines if x =='[gpio_para]']
[x for x in lines if x ==b'[gpio_para]']
[x for x in lines if x ==b'']
list(in_between(b'[gpio_para]', b'', lines))
n
def in_between(start, end, lines):
    t = 0
    for l in lines:
        if l == start:
            t += 1
        if t == 1 and l == end:
            t += 1
        if t == 1:
            yield l

list(in_between(b'[gpio_para]', b'', lines))
