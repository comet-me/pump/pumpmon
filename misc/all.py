#!/usr/bin/env python

#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
from subprocess import check_output
from time import sleep, time


INPIN = 'PH0' # PH0 is on EINT0 but already used, could not export with gpio-sunxi
OUTPIN = 'PG0'


def reader_mem_polling():
    from pyA20.gpio import gpio, port, connector
    gpio.init()
    inpin = getattr(port, INPIN)
    gpio.setcfg(inpin, gpio.INPUT)
    print("reader polling from {INPIN} = {inpin}".format(INPIN=INPIN, inpin=inpin))

    changes = []
    old = gpio.input(inpin)
    while len(changes) < 10000:
        val = gpio.input(inpin)
        if val != old:
            changes.append(time())
            old = val
        sleep(0.0005)

    postprocess(changes)


def reader_poll():
    if not os.path.exists('/sys/class/gpio'):
        print("missing gpio-sunxi module")
        raise SystemExit

    from pygpiosunxi import GPIO, RISING
    inp = GPIO(pin=INPIN, interrupt=RISING)
    changes = []
    print('reader from {INPIN}'.format(INPIN=INPIN))
    for t in inp.poll_iter():
        # since we used RISING, this is when the pulses start
        changes.append(t)
        if len(changes) == 10000:
            break
    postprocess(changes)


def postprocess(changes):
    import numpy as np
    dt = np.diff(np.array(changes))
    print('max {max} min {min}'.format(max=dt.max(), min=dt.min()))
    #with open('times.txt', 'w+') as fd:
    #    fd.write('\n'.join([str(x) for x in changes]) + '\n')


def writer_mem():
    from pyA20.gpio import gpio, port, connector
    gpio.init()
    outpin = getattr(port, OUTPIN)
    gpio.setcfg(outpin, gpio.OUTPUT)
    print("writer to {OUTPIN} = {outpin}".format(OUTPIN=OUTPIN, outpin=outpin))

    out = 0
    while True:
        gpio.output(outpin, out)
        out = 1 - out
        sleep(0.001)

def writer_mem():
    from pygpiosunxi import GPIO
    outp = GPIO(pin=OUTPIN)
    print("writer to {OUTPIN}".format(OUTPIN=OUTPIN))

    out = 0
    while True:
        outp.output(out)
        out = 1 - out
        sleep(0.001)



if __name__ == '__main__':
    basename = os.path.splitext(os.path.split(sys.argv[0])[1])[0]
    if basename == 'reader_polling':
        reader_mem_polling()
    elif basename == 'reader_poll':
        reader_poll()
    else:
        writer()
