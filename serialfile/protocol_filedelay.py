#!/usr/bin/env python3

#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This module implements a debug only file reading serial port.

import os
from time import sleep
from serial.serialutil import SerialBase, SerialException, portNotOpenError


class Serial(SerialBase):
    """Serial port implementation reading from a file."""

    def open(self):
        """\
        Open port with current settings. This may throw a SerialException
        if the port cannot be opened.

        portstr is expected to be:
        file://<filename>#<delay-in-seconds>
        """
        if self._port is None:
            raise SerialException("Port must be configured before it can be used.")
        if self.is_open:
            raise SerialException("Port is already open.")
        hash_ind = self.portstr.index('#')
        self.delay = float(self.portstr[hash_ind + 1:]) # seconds, floating point
        self.filename = self.portstr[len('filedelay://'):hash_ind]
        self.filesize = os.stat(self.filename).st_size
        try:
            # timeout is used for write timeout support :/ and to get an initial connection timeout
            self._fd = open(self.filename, 'rb')
        except Exception as msg:
            self._fd = None
            raise SerialException("Could not open file {}: {}".format(self.portstr, msg))

        self.is_open = True

    def _reconfigure_port(self):
        """\
        Set communication parameters on opened port. For the file://
        protocol all settings are ignored!
        """
        pass

    def close(self):
        """Close port"""
        if not self.is_open:
            return
        self._fd.close()
        self.is_open = False

    @property
    def in_waiting(self):
        """Return the number of bytes currently in the input buffer."""
        if not self.is_open:
            raise portNotOpenError
        return self.filesize - self._fd.tell()

    # select based implementation, similar to posix, but only using socket API
    # to be portable, additionally handle socket timeout which is used to
    # emulate write timeouts
    def read(self, size=1):
        """\
        Read size bytes from the serial port. If a timeout is set it may
        return less characters as requested. With no timeout it will block
        until the requested number of bytes is read.
        """
        if not self.is_open:
            raise portNotOpenError
        # put the delay right before the EOL
        ret = self._fd.read(size)
        delay = ret.count(b'\n') * self.delay
        if delay > 0:
            sleep(delay)
        return ret

    def write(self, data):
        """
        No writing for file:// ; this method raises an exception
        """
        raise SerialException("file:// does not support writing")

    def send_break(self, duration=0.25):
        """
        no break sent for file://
        """
        if not self.is_open:
            raise portNotOpenError
        if self.logger:
            self.logger.info('ignored send_break({!r})'.format(duration))

    def _update_break_state(self):
        """Set break: Controls TXD. When active, to transmitting is
        possible."""
        if self.logger:
            self.logger.info('ignored _update_break_state({!r})'.format(self._break_state))

    def _update_rts_state(self):
        """Set terminal status line: Request To Send"""
        if self.logger:
            self.logger.info('ignored _update_rts_state({!r})'.format(self._rts_state))

    def _update_dtr_state(self):
        """Set terminal status line: Data Terminal Ready"""
        if self.logger:
            self.logger.info('ignored _update_dtr_state({!r})'.format(self._dtr_state))

    @property
    def cts(self):
        """Read terminal status line: Clear To Send"""
        if not self.is_open:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for cts')
        return True

    @property
    def dsr(self):
        """Read terminal status line: Data Set Ready"""
        if not self.is_open:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for dsr')
        return True

    @property
    def ri(self):
        """Read terminal status line: Ring Indicator"""
        if not self.is_open:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for ri')
        return False

    @property
    def cd(self):
        """Read terminal status line: Carrier Detect"""
        if not self.is_open:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for cd)')
        return True

    # - - - platform specific - - -

    # works on Linux and probably all the other POSIX systems
    def fileno(self):
        """Get the file handle of the underlying socket for use with select"""
        return self._fd.fileno()


#
# simple client test
if __name__ == '__main__':
    import serial
    import sys
    serial.protocol_handler_packages.append('serialfile')
    s = Serial('filedelay:///etc/passwd#{last}'.format(last=sys.argv[-1]), baudrate=57600)
    while True:
        line = s.readline()
        if len(line) == 0:
            break
        print(line)
