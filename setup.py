#!/usr/bin/env python3

#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup

from pumpmon import VERSION

setup(
    name='pumpmon',
    description='Pump monitoring utilities',
    version='.'.join(map(str, VERSION)),
    setup_requires=[],
    # Note: on Raspberry-pi we use RPi.GPIO - but we rely on the external
    # (debian) packaging to install it.
    install_requires=[
        'pyserial',
        'emolog',
    ],
    packages=['pumpmon', 'pumpmon.gpio'],
    data_files=[],
    entry_points={
        'console_scripts': [
            'record_controller = pumpmon.record_controller:main',
            'record_flow = pumpmon.record_flow:main',
            'record_mppt = pumpmon.record_mppt:main',
        ]
    },
    dependency_links=[
      'git+https://github.com/alon/emolog.git#egg=emolog-1.0&subdirectory=emolog_pc',
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPL License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
    ],
)
