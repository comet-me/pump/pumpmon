#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pdb import set_trace as b
from subprocess import Popen
from pathlib import Path
from glob import glob
from time import sleep
from linecache import getlines
from os import system, path, getcwd, chdir, makedirs
from shutil import rmtree
from tempfile import TemporaryDirectory
from contextlib import contextmanager


@contextmanager
def TemporaryNoCleanup():
    d = '/tmp/mytempdirectory'
    if path.exists(d):
        rmtree(d)
    makedirs(d)
    yield d


def with_temp_dir(cmdline_fmts, runtime=None, setup=None, leave=False):
    with TemporaryNoCleanup() if leave else TemporaryDirectory() as d:
        if callable(setup):
            cwd = getcwd()
            chdir(d)
            setup()
            chdir(cwd)
        cmdline = [f.format(d=d) for f in cmdline_fmts]
        if runtime is None:
            system(' '.join(cmdline))
        else:
            proc = Popen(cmdline)
            sleep(runtime)
            proc.kill()
            sleep(0.1)
            proc.poll()
            if proc.returncode is None:
                proc.terminate()
        files = glob(str(Path(d) / '*' / '*' / '*' / '*.csv'))
        contents = [getlines(path.join(d, x)) for x in files]
        return files, contents
