#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import importfix
import io
from datetime import datetime

from pumpmon.rotatingcsv import DailyCSVWriter, from_filename


class MyStringIO(io.StringIO):
    def close(self):
        pass


def test_daily_csv_writer():
    files = {}
    dates = [
        datetime(year=2020,month=1,day=2, hour=1, minute=1),
        datetime(year=2020,month=1,day=2, hour=1, minute=2),
        datetime(year=2020,month=1,day=3, hour=2, minute=1),
        datetime(year=2020,month=1,day=3, hour=2, minute=2),
        datetime(year=2020,month=1,day=3, hour=3, minute=3),
        datetime(year=2020,month=1,day=3, hour=3, minute=4),
    ]
    class Tester(DailyCSVWriter):
        now_results = dates
        now_i = 0
        def _next_now(self):
            self.now_i += 1
        def _now(self):
            ret = self.now_results[self.now_i]
            return ret
        def _append_to_file(self, filename):
            if filename not in files:
                files[filename] = MyStringIO()
            ret = files[filename]
            ret.seek(0, 2) # seek to end of stream
            return ret
    t = Tester(prefix='test', fields=['a','b'])
    t.writeheader()
    for row in [
        {'a':'1', 'b': 'a'},
        {'a':'2', 'b': 'b'},
        {'a':'3', 'b': 'c'},
        {'a':'4', 'b': 'd'},
        ]:
        t.writerow(row)
        t._next_now()
    # now simulate a writeheader by the user
    t.writeheader()
    t._next_now()
    t.writerow({'a':'5', 'b': 'e'})

    # Simulate a new header but no row - should not augment the file count
    t.writeheader()
    assert len(files) == 3
    expected_results = [
        ('2020/01/02/test_20200102_0101.csv', 'a,b\r\n1,a\r\n2,b\r\n'),
        ('2020/01/03/test_20200103_0201.csv', 'a,b\r\n3,c\r\n4,d\r\n'),
        ('2020/01/03/test_20200103_0304.csv', 'a,b\r\n5,e\r\n')
        ]
    expected_names = {name for name, _ in expected_results}
    assert set(files.keys()) == expected_names
    for name, value in expected_results:
        assert files[name].getvalue() == value


def test_from_filename():
    writer = from_filename('emo_001.csv', fields=['a', 'b'], lineterminator='\n')
    assert writer.prefix == 'emo'
    assert writer.writer_kw == dict(lineterminator='\n')

