#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import importfix
from datetime import datetime
import pytest

from pumpmon.mppt import MPPTLogParser


mppt_test_data = [
    (b'\n', None),
    (b'MPPT:    Vi=36.30, Ii=5.92,   Vo=37.00, Io=5.65,   Pi=214, Po=208,   D=6, n=97.17, Tmp.=35	 Flgs: OV=0, OT=0, TF=0, OFF=0, UV=0	 Time: 1860544\n',
    {
        b'State': b'MPPT',
        b'Vi':b'36.30', b'Ii':b'5.92', b'Vo':b'37.00', b'Io':b'5.65', b'Pi':b'214', b'Po':b'208',
        b'D':b'6', b'n':b'97.17', b'Tmp.':b'35',
        b'Flgs': {
            b'OV':b'0', b'OT':b'0', b'TF':b'0', b'OFF':b'0', b'UV':b'0'
        },
        b'Time': b'1860544'
    },
    ),
    (b'Compiled: Feb 19 2017, 11:33:30, 4.9.2, C:\\Users\\Zvi Schneider\\Dropbox\\Comet-Me\\MPPT control\\ATMEL Arduino\\MPPT_calibration\\MPPT_calibration.ino\n',
    {
        b'Line': b'Compiled: Feb 19 2017, 11:33:30, 4.9.2, C:\\Users\\Zvi Schneider\\Dropbox\\Comet-Me\\MPPT control\\ATMEL Arduino\\MPPT_calibration\\MPPT_calibration.ino'
    }),
]


@pytest.mark.parametrize('line,output', mppt_test_data)
def test_mppt(line, output):
    # break it up any way possible. Multiply it several times.
    N = 4
    data = line * N
    # feed it everything in a single byte at a time, just like serial does
    one_at_a_time = lambda d: [[d[i:i+1] for i in range(len(d))]]
    two_pieces = lambda d: [[d[:i], d[i:]] for i in range(len(data))]

    for parts in one_at_a_time(data) + two_pieces(data):
        parser = MPPTLogParser()
        msgs = sum([parser.parse_message(p) for p in parts], [])
        if output is None:
            assert len(msgs) == 0
        else:
            assert len(msgs) == N
            # we use datetime, so just skip the value (instead of mocking datetime)
            for m in msgs:
                assert b'RecordedTime' in m
                recorded_time = m[b'RecordedTime']
                assert type(recorded_time) is float, "RecordedTime is not of type float"
                now_milli = datetime.now().timestamp() * 1000
                diff = abs(now_milli - recorded_time)
                assert diff < 10, "time is not recorded in milliseconds since epoch, UTC - difference is: {}".format(diff)
                del m[b'RecordedTime']
                assert m == output


if __name__ == '__main__':
    parser = MPPTLogParser()
    print(parser.parse_message(mppt_test_data[-1][0]))
