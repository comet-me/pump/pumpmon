#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pdb import set_trace as b
from tempfile import TemporaryDirectory
from pathlib import Path
from linecache import getlines
from time import sleep

from pumpmon.record_mppt import dict_consume

from util_test import with_temp_dir


def test_dict_consume():
    for pre, key, post, val in [
        ({}, b'a', {}, None),
        ({b'a':b'b'}, b'a', {}, b'b'),
        ({b'a':{b'b':b'b', b'c':b'c'}}, b'a.b', {b'a':{b'c':b'c'}}, b'b'),
        ({b'a':{b'b':b'b'}}, b'a.b', {}, b'b'),
        ({b'a':{b'b':b'b'}, b'A':b'1'}, b'a.b', {b'A':b'1'}, b'b'),
        ]:
        res_val = dict_consume(pre, key)
        assert res_val == val
        assert post == pre


def wofirst(l):
    return l.split(',', 1)[1]


def test_record_mppt_delay():
    csvfiles, csv = with_temp_dir(['record_mppt', '--dir', '{d}', '--url', 'filedelay://logs/MPPT_2_seconds.log#1.0'])

    assert all([x.endswith('.csv') for x in csvfiles])
    assert len(csvfiles) == 1
    data = csv[0]
    assert len(data) == 3
    assert data[0] == 'RecordedTime,Time,State,Vi,Ii,Vo,Io,Pi,Po,D,n,Tmp,Flgs.OV,Flgs.OT,Flgs.TF,Flgs.OFF,Flgs.UV,Unparsed\n'
    assert [wofirst(l) for l in data[1:]] == [
        '1860544,MPPT,36.30,5.92,37.00,5.65,214,208,6,97.17,35,0,0,0,0,0,\n',
        '1861970,MPPT,37.55,5.37,38.12,5.16,201,196,7,97.62,35,0,0,0,0,0,\n',
    ]


def record_mppt_fake(source):
    files, contents = with_temp_dir(['record_mppt', '--dir', '{d}', '--url', '"file://%s"' % source])
    return {f: c for f, c in zip(files, contents)}


def enumerate_mod(lines):
    for i, l in enumerate(lines):
        if len(l) == 0:
            yield i, l
        else:
            yield i % len(l), l


def record_mppt_fake_with_errors(source):
    lines = [x.strip() for x in getlines(str(source))]
    assert len(lines) > 0
    lines_with_error = [x[:i] + '\n' + x[i:] + '\n' + x + '\n' for i, x in enumerate_mod(lines)]
    assert len(lines_with_error) == len(lines)
    with TemporaryDirectory() as tempdir:
        filename = str(Path(tempdir) / 'mppt.log')
        with open(filename, 'w+') as fd:
            fd.writelines(lines_with_error)
            fd.flush()
        files, contents = with_temp_dir(['record_mppt', '--dir', '{d}', '--url', f'"file://{filename}"'])
        d = {f: c for f, c in zip(files, contents)}
        d['orig'] = lines_with_error
        return d


def helper_test_record_mppt_recording(source, expected):
    d = record_mppt_fake(source)
    assert len(d) == 1
    data = list(d.values())[0]
    baseline = getlines(expected)
    if baseline != data:
        print("new baseline?:")
        print(''.join(data))
    assert [wofirst(l) for l in baseline] == [wofirst(l) for l in data]


def test_record_mppt_recording():
    for source, expected in [
            ( 'logs/MPPT_log_height_15_2017_11_28 14_24_49.log', 'logs/test_record_mppt_recording.baseline',),
            ( 'logs/no_load_example.log', 'logs/no_load_example.baseline',),
        ]:
        helper_test_record_mppt_recording(source, expected)


def contained(subset, complete):
    """
    Return True if subset is in order appearing in complete
    """
    n = len(subset)
    i_comp = 0
    for l in subset:
        while i_comp < len(complete):
            if complete[i_comp] == l:
                break
            i_comp += 1
        else:
            return False
    return True


def test_contained():
    assert contained(['a', 'b'], ['a', 'A', 'b'])
    assert not contained(['a', 'b'], ['a'])
    assert not contained(['a', 'b'], ['b'])
    assert not contained(['a', 'b'], ['c'])
    assert not contained(['a', 'b'], [])


def test_record_mppt_recording_with_errors():
    source, baseline = 'logs/MPPT_log_height_15_2017_11_28 14_24_49.log', 'logs/test_record_mppt_recording.baseline'
    d = record_mppt_fake_with_errors(source)
    assert len(d) == 2
    assert 'orig' in d
    orig = d.pop('orig')
    data = list(d.values())[0]
    baseline = getlines(baseline)

    base_no_first_word = [wofirst(l) for l in baseline]
    data_no_first_word = [wofirst(l) for l in  data]
    is_contained = contained(base_no_first_word, data_no_first_word)
    if not is_contained:
        print("new baseline?:")
        print(''.join(data))
    assert is_contained


if __name__ == '__main__':
    #test_dict_consume()
    test_record_mppt_recording()
    #test_record_mppt_delay()
