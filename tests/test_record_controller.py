#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import importfix
import os

from util_test import with_temp_dir


def record_controller_fake():
    def setup():
        with open('vars.csv', 'w+') as fd:
            fd.write("""controller.state.analog_sensors.temp_ext,20000,0
controller.state.last_flow_rate_lpm,20000,0
controller.state.analog_sensors.temp_a,20000,0
controller.state.analog_sensors.temp_b,20000,0
controller.state.analog_sensors.temp_c,20000,0
controller.state.position,20000,0
controller.state.step_time_prediction,20000,0
controller.state.commutation_sensors,20000,0
controller.state.ref_sensor,20000,0
controller.state.motor_state,20000,0
controller.state.required_dir,20000,0
controller.state.actual_dir,20000,0
controller.state.analog_sensors.dc_bus_v,20000,0
controller.state.analog_sensors.total_i,20000,0
controller.state.duty_cycle.duty_cycle,20000,0
controller.state.mode,20000,0
""")
    filenames, contents = with_temp_dir('record_controller --dir {d} --fake host --vars vars.csv', runtime=2.0, setup=setup)
    both = [(x, xc) for x, xc in zip(filenames, contents) if x.endswith('.csv')]
    return dict(both)


def test_sanity():
    d = record_controller_fake()
    both = {k: v for k, v in d.items() if k != 'vars.csv'}
    assert len(both) == 1
    filename, lines = list(both.items())[0]
    assert filename.startswith('pump_')
    assert len(lines) > 3, '{lines}, {filename}'.format(lines=repr(lines), filename=repr(filename))
    with open('temp.tmp', 'w+') as fd:
        for line in lines:
            fd.write(line)


if __name__ == '__main__':
    test_sanity()
