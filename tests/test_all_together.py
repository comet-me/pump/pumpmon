#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import importfix
from datetime import datetime
from multiprocessing import Process, Pipe

from test_record_flow import record_flow_mocked_gpio
from test_record_controller import record_controller_fake
from test_record_mppt import record_mppt_fake


def pipeit(f):
    def wraps(p):
        print("start")
        try:
            data = f()
        except Exception as e:
            data = e
        p.send(data)
        print("closing")
        p.close()
    return wraps


def helper():
    return 432


def test_times_match():
    fut = [
              record_flow_mocked_gpio,
              record_controller_fake,
              record_mppt_fake
          ]
    pipes = [Pipe() for i in range(len(fut))]
    processes = [Process(target=pipeit(f), args=(child,)) for (parent, child), f in zip(pipes, fut)]
    start = datetime.now().timestamp() * 1000
    for p in processes:
        p.start()
    results = [parent.recv() for (parent, child) in pipes]
    flow, controller, mppt = results
    for p in processes:
        p.join()

    wmp_times = [x['time'] for x in flow]
    controller_times = [l.split(',')[2] for l in list(controller.values())[0][1:]]
    mppt_times = [l.split(',')[0] for l in list(mppt.values())[0][1:]]
    wmp_start = wmp_times[0]
    controller_start = controller_times[0]
    mppt_start = mppt_times[0]
    times = [float(t) - start for t in [wmp_start, controller_start, mppt_start]]
    assert max(times) - min(times) < 5000, 'wmp, pump, mppt start times = {}'.format(times)


