#    Recording of Comet-ME Pump related data, including pump controller,
#    MPPT device and Flow meter.
#    Copyright (C) 2018  Comet-ME
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import importfix
import functools
from unittest.mock import MagicMock, patch
from time import sleep
from datetime import datetime

from pumpmon.record_flow import record
from pumpmon.gpio.pygpiosunxi import ON_BOARD

from util_test import with_temp_dir


def gen_pulses(dts):
    time = 42
    yield {'time': time, 'dt': dts[0]}
    for dt in dts:
        sleep(dt)
        time += dt
        yield {'time': time, 'dt': dt}


def record_flow_mocked_gpio(dts=None):
    if dts is None:
        dts = [0.1, 0.1, 0.05]
    @patch('pumpmon.record_flow.DailyCSVWriter')
    @patch('pumpmon.record_flow.pulsegen')
    def record_flow_mocked_gpio_helper(pulsegen, DailyCSVWriter):
        pulsegen.return_value = gen_pulses(dts)
        DailyCSVWriter.return_value = MagicMock()
        record()
        DailyCSVWriter.assert_called_with(fields=['time', 'rate', 'dt', 'pulses'], prefix='flow')
        return DailyCSVWriter.return_value.writerow.mock_calls
    mock_calls = record_flow_mocked_gpio_helper()
    return [v[1][0] for v in mock_calls]


def close(a, b, maxdiff, do_assert=True):
    cond = abs(a - b) < maxdiff
    if do_assert:
        assert cond, '{a} - {b} >= {maxdiff}'.format(a=a, b=b, maxdiff=maxdiff)
    return cond


def test_record_flow_mocked_gpio():
    dts = [0.1, 0.1, 0.05]
    start = datetime.now().timestamp() * 1000
    res = record_flow_mocked_gpio(dts)

    cumsum = functools.reduce(lambda s, n: s + [s[-1] + n], dts, [start])[1:]

    res_time = [t['time'] for t in res]
    res_rate = [t['rate'] for t in res]

    assert all([close(time, c, maxdiff=400) for time, c in zip(res_time, cumsum)]),\
        'got: {res}, cumsum {cumsum}'.format(res=repr(res), cumsum=cumsum)

    assert all([close(rate, 1.0 / d, maxdiff=2e-1, do_assert=False) for rate, d in zip(res_rate, dts)]),\
        'got: {res_time}, {res_rate}'.format(res_time=repr(res_time), res_rate=repr(res_rate))


if ON_BOARD:
    def run_hardware_test():
        filenames, contents = with_temp_dir('record_flow --dir {d}', runtime=3.0)
        assert len(filenames) == 1
        assert len(contents) == 1
        lines = contents[0]
        assert len(lines) > 10, '{lines}, {filenames}, {contents}'.format(lines=repr(lines), filenames=repr(filenames), contents=repr(contents))
        with open('temp.tmp', 'w+') as fd:
            for line in lines:
                fd.write(line)


if __name__ == '__main__':
    if ON_BOARD:
        print("running hardware test: make sure PH0 is connected to something giving at least one pulse a second")
        run_hardware_test()
    else:
        test_record_wmp101_mocked_gpio()
